﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Killer
{
    class Killer
    {
        public static bool Hunter(int id, TimeSpan border)
        {
            if (DateTime.Now-ProcBase.GetTime(id)>=border)
            {
                if (!Process.GetProcessById(id).HasExited)
                {
                    Process.GetProcessById(id).Kill();
                    Console.WriteLine("Процесс с ID {0}, был убит",id);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
