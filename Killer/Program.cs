﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace Killer
{
    class Program
    {
        static void Main(string[] args)
        {
            ProcBase.Add();
            int looking = Seach.UnicProcMaker(args[0]);
            while (looking ==-1)
            {
                Environment.Exit(0);
            }
            int spanCheck;
            int spanLife;
            bool processAlive=true;
            bool a = int.TryParse(args[1], out spanLife);
            bool b = int.TryParse(args[2], out spanCheck);
            TimeSpan interval = new TimeSpan(0, spanCheck,0);
            TimeSpan border = new TimeSpan(0, spanLife, 0);
            while (processAlive)
            {
                if (a&&b)
                {
                    processAlive = Killer.Hunter(looking, border);
                    Thread.Sleep(interval);
                }
                else
                {
                    Console.WriteLine("Не верно задан срок жизни или интервал проверки");
                    Environment.Exit(0);
                }
            }
        }
    }
}
