﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Killer
{
    class Seach
    {
        static public Dictionary<int,string> ProcFinder(string proc, List<int> processID, List<string> processName)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            if (processID.Count == processName.Count)
            {
                for (int i = 0; i < processName.Count; i++)
                {
                    if (EqualsString(proc, processName[i]))
                    {
                        result.Add(processID[i], processName[i]);
                    }
                }
            }
            return result;
        }
        static public int UnicProcMaker(string processName)
        {
            Dictionary<int, string> LookProc = ProcFinder(processName, ProcBase.GetID(), ProcBase.GetName());
            int Looking = 0;
            if (LookProc.Count == 1)
            {
                Looking = LookProc.Keys.First();
            }
            else if (LookProc.Count == 0)
            {
                Console.WriteLine("Такого процесса не найдено");
                Looking = -1;
            }
            else
            {
                Console.WriteLine("Найдено больше одного процесса, укажите ID нужного");
                foreach (var item in LookProc)
                {
                    Console.WriteLine("ID process: {0} Name Process: {1}", item.Key, item.Value);
                }
                string ui;
                while (!int.TryParse(Console.ReadLine(), out Looking))
                {
                    ui = Console.ReadLine();
                }
            }
            return Looking;
        }
        static bool EqualsString(string a, string b)
        {
            bool result = true;
            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    if (result)
                    {

                        if (a[i] == b[i])
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}
