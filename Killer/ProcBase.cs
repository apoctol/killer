﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Killer
{
    class ProcBase
    {
        static List<int> processID = new List<int>();
        static List<string> processName = new List<string>();
        static Dictionary <int,DateTime> processDateStart = new Dictionary <int,DateTime>();
        static public void Add()
        {
            foreach (Process process in Process.GetProcesses())
            {
                processID.Add(process.Id);
                processName.Add(process.ProcessName);
                try
                {
                    processDateStart.Add(process.Id, process.StartTime);
                }
                catch (Exception)
                {
                    processDateStart.Add(process.Id, DateTime.Now);
                }
                
            }

        }
        static public void Clear()
        {
            processID.Clear();
            processName.Clear();
        }
        public static DateTime GetTime(int id)
        {
            return processDateStart[id];
        }
        public static List<int> GetID()
        {
            return processID;
        }
        public static List<string> GetName()
        {
            return processName;
        }

    }
}
